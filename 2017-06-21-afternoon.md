# Opening on H20 Open Research Publishing Platform

[Dr. Jean-Claude Burgelman]

**Open Science, from vision to action**

1. Open Data
1. Science cloud
1. Altmetrics
1. Scholarly communication: peer reviewed, open access

FP7 to H2020: all OA, avec des embargo 6/12, gold, encouragement aux auteurs de garder leurs droits et d'offrir des licences

Actuellement 60-68% OA, donc encore des efforts pour atteindre les buts

Ouvrir tout le workflow de l'édition académique

- Proposition (ORE, Open Access Europe):
    - launch own publication platform
    - pour H202, preprints server, Open Peer Review, …
    - pourrait devenir une 3rd way pour l'OA
- Implementation:
    - n'ont pas d'expérience en tant qu'éditeur
    - high quality standards
    - outsourced? [by commercial editors?]

# Tech. Session `#1`: Making a case for decentralised scholarly communication

http://csarven.ca/presentations/dokieli-oai

- freedom of expression
    - dans le domaine des articles, des review
    - annotations
    - d'habitude des objets 2D, anti-social, mais now
    - we have the web!
    - + semantics
    - donner aux possesseurs de la connaissance le meilleur moyen de le partager
- interoperability
    - reusable
    - les données produites par un tool construit today seront utilisées par des outils demain si se base sur les standards, genre W3C
- decentralisation
    - le web le permet, or la publication scientifique est concentrée sur quelques plateformes et les plateformes sont disjointes, non fédérées
- accessibility
    - pour l'humain et la machine
- assumptions
    - toutes les interactions se basent sur de standards ouverts
    - au moins deux différentes implémentations interopérables
    - accessible (licence, et a11y, semantics)
    - le faire sans se mettre d'accord sur tout à l'avance

# Tech. Session `#2`: Interoperability and Data FAIRness emerges from a novel combination of Web technologies

http://tinyurl:WilkinsonOAI10

Mainly data are reuse… less!

Findable Accessible Interoperable Reusable: FAIR

- Interoperable:
    - → REST API
    - FAIR Accessor, donne accès aux metadata through REST
    - possible de donner un ID à une requête
    - en fait ça ressemble bcp à singposting, sous forme d'une REST

# Tech. Session `#3`: The Web is not a glorified USB stick

https://www.w3.org/2017/Talks/0621-phila-oai/

W3C: Data on the web best practices: documenter how to use correctly the web of data publishing or reuse

https://www.w3.org/TR/2017/REC-dwbp-20170131/

Spatial Data on the Web best practices, plus spécifiques

Use schema.org for discoverability! Nothing else!, et seulement pour ça.

→ Data quality vocabulary (note de WG), permet par ex. de décrire les condition de reusability, aspect de licence, plus précisément

→ Metadata Vocabulary for Tabular Data "CSV on the Web". Mais besoin de décrire plus précisément la table, format, OS... C'est un JSON qui explique comment processer le CSV

Shapes Constraint Language (SHACL): permet de valider du RDF avec un schema

Chacun utilise son propre set de metadata, même en respectant les standards… → data exchange working group charter.

Conclusion: le web est pour échanger des informations, des données, pas des documents! Don't use the web as a glorified USB stick! Il faut donc décrire, définir, chaque donnée, par des documents, pas des PDF, pas des ZIP files

2 faiblessse dans schema.org:
    - la taille, trop grand
    - dépend trop d'une personne remarquable…

# Tech. Session `#4` Copyright reform for education

Diego Gomas en procès pour avoir partagé un article dont il n'avait pas les droits. 4-8 ans de prison. Nouvelle loi après un accord commercial avec US (qui comprennent toujours des clause sur ©).

OA is a way to get full access to knowlegde & education pour une société ouverte. Mais sans des bonnes règles de ©, ça va être difficile.

CreativeCommons: ça ne règle pas le problème, ça donne plus de souplesse, c'est tout. Avec un © idéal, alors on a pas besoin de licences ouvertes, on aurait le droit de partager, remix…

© reform in EU. Les anciennes règles sont très vieilles 2001, avant FB… Donc il faut une réforme. Mais la réforme est très obtuse. Faire pression sur les opérateurs et les ISPs de monitorer et filter avant publication. Fin du Web tel qu'on le connaît.

N'adapte le © adapté aux mœurs du XXI^e siècle, mais on étend le ©, on le renforce.

Pas pris en compte les contenus générés par les utilisateurs.

On a besoin d'exceptions aux ©. Et qui est concerné par les exceptions.

Freedom to teach, et ce n'est pas le cas à cause du ©. Les exceptions en EU sont différentes d'un pays à l'autre et c'est donc difficile de savoir ce qu'on a le droit de faire, par exemple en ligne (MOOC)…

FR, DK, il est interdit de faire des photocopies de textes en classe.

Le but peut être *démontré* par la situation de l'Estonie.

http://rightcopyright.eu/why-now

# Tech. Session `#5`: Rights as foundation of scholarly communication



# Tech. Session `#6`: Legal aspect of Text and Data Mining --- A current of European outlook

LIBER, représentant 400 lib, 40 pays.


