---
title: Workshop on signposting and resourcesync

---

# T.2.. Interoperability for Discovery and Navigation: ResourceSync and Signposting

2 axes, navigation et batch

## Navigation: Signposting

- not a standard, to simple to need it
- typed weblink (in the header)
    - 2 URI, l'une décrit la deuxième
        - un GET → `link: <URI-2>; rel="describes"`
        - being used
        - on peut définir que c'est une licence
        - ou alors informer que l'info est aussi dans un format alternatif
        - ou alors un timegate dans memento
    - tous ces termes sont définis dans des RFC (licence, describes, …)
    - on peut avoir nos propres relation type, mais avec une URI qui la définit, mais c'est évidemment moins connu par le monde entier (les autres rep.)
    - applicable pour n'importe quel ressource, n'importe quel MIME types
    - accessible avec le HEAD, sans avoir besoin du contenu: c'est l'idée du signposting, parce qu'on sait où aller ensuite sans accéder au contenu
    - efficace pour les grands sets
    - accessible même si la ressource est en accès limité
    - en cours, le type linkset qui indique une ressource contenant tous les linkrel
- résoudre des problèmes précis
    - pid: une gd quantité de scholarly res avec doi, ne sont pas référencés par leur doi, parce qu'on partage l'URL, mais pas le doi, qui souvent est caché
        - une fois qu'on est sur le html ou le pdf, comment on connaît le PID? → avec un weblink type! Permettre de bookmarker, ou zoteroiser le doi et pas l'URL actuelle
        - comment embarquer les éditeurs là dedans, mais en attentant les IR
        - `rel="identifier"` pour pointer depuis les contenus, la page html
    - limite de publication: dans une landing page, beaucoup de liens dont très peu vers le contenu de la ressource elle-même et c'est lourd en euristiques pour les retrouver
        - `rel="item" type="application/pdf"`, `rel="collection"`
        - si on connaît le type des liens, alors on sait lesquels sont les bons
        - on peut même ajouter un mime type, qui permet depuis le doi de trouver le fichier qu'on recherche, par exemple le PDF
    - bibliogr. metadata: dans de multiples formats, rangées un peu n'importe tout, donc construire des euristiques pour chaque portal... → `rel="describes" rel="describedby"` qui pointe vers les données ou des données vers la ressource
        - convention de mime types de bibliogr. metadata, pour normaliser les mime types
        - bcp de XML pour des set de metadonnées différents, donc un attribut profile pour spécifier quel xml c'est
    - existe aussi le `rel="author"`
- besoin de définir quel type d'objet un doi référence: article, data, peer-review, …, par exemple via schema.org
- vers quoi pointe exactement le doi, vers l'html, l'"article" ? les doi ne sont pas adaptés au web… "PID and the Web, the need for a good mapping", un article à chercher
- pourrait aider à identifier les contenus text minable, par exemple avec le rel="alternate?", mais resourcesync est plus efficace pour ça

## ResourceSync: A Modular Framework for Web-Based Resource Synchronization

- standard
- OAI-PMH, 1999-2002, daté
    - seulement pour l'échange de données XML
    - pas adapté au web
- ne pas renouveler PMH, mais faire un truc neuf, webish
- resource centric VS IR centric
- basé sur les sitemaps
- problem:
    - a source (a server) has resources (anything that has a URI) that change over time
    - destination (application) are using resources of the source, besoin d'être à jour
- goal: a web-based approch to communicate the changes
- échelle:
    - petites collections, ou des IR complets
- fréquences des changements: très rapide (chaque secondes), ou plus lentes
- latency: le délai de sync, immédiatement ou chaque semaine?
- needs at the destination:
    - pour toute la collection?
    - seulement une partie?
- vérification de l'authenticité des ressources (bitstream accuracy)
- d'un environnement à un autre, ou alors un multi mirroring
- ou alors une agrégation, de plusieurs sources vers une destination
- sync sélective: ne prendre que le full text
- metadata harversting: sur le web les metadata ont une URI, donc sync'able
- destination
    - baseline sync: la première fois, l'initialisation
    - incremental sync: stay in touch avec les changements
    - audit: vérifier si ça marche, si on a ce qu'on est censé avoir (coverage, bitstream)
- source:
    - publish inventory: photo à un moment donné, liste d'URI
    - publish change: liste d'URI des changements entre t1, t2, tn+1
    - notify about change: pousser les notifications (push, not pull)
    - communication: à la base l'URI de la ressources, puis le reste (changement...)
- inventory: resource list (sitemap)
    - énumère les resources
    - au moins les URI
    - pour le baseline sync
    - on peut empaqueter le contenu de l'inventory: resource dump, peut faciliter le baseline sync de grandes collections
    - la resource list, présente ce qui existe au moment t et pas t-1 ou t+1 (donc pas les contenu supprimés par exemple)
- change list
    - tous les changements dans un espace temporel
    - avec leurs URI, et la nature des changement
    - le dump des changements existe aussi
    - si une ressource change 2x, alors les deux changements seront indiqués
- notification
    - comme la change list, mais pushed (via subsciption (websub, pubsubhubub))
    - doit forcément indiqué l'espace temps concerné
- Communication payload, metadata and links
    - metadata décrivant le contenu, checksums
    - des links pour n'importe quoi, par exemple le diff, sont forcément des relations…
- caractéristiques du framework
    - modular, on peut choisir les fonctionnalités qu'on veut implémenter, selon les besoins de sa communauté
    - sets of resources, comme dans PMH (URIs list)
    - discovery: publier le type d'implémentation, ce qu'on y trouve, comment...
- open change list: une ressource qui est constamment updatée jusqu'à un état stable: le change list de cette resource est open jusqu'au moment stable, c'est incremental
- Technology Review
    - sitemaps: le format au centre du framework
        - un sitemap avec des annonces ResourceSync
        - possibilité d'utiliser les sitemap index, pour proposer plusieurs sitemaps, avec un index qui les référence
        - ajout d'un namespace rs, avec des ln (links) et md (metadata)
    - resource list
        - `rs:md capability="resourcelist" at="isodate"`
        - `rs:md hash="" length="" type="text/html"`
        - `rs:ln rel="index" href="URL"` vers l'index, par exemple
    - change notifications
        - pour réduire la latency et évite de pull constamment
        - via websub (PubSubHubbub)
        - aurait pu utiliser xmpp, mais le problème d'ouvrir les ports nécessaires, alors que websub est sur les ports habituels du web
        - `rs:md capability="changelist-notification" from="" until=""`
        - `rs:md change="created" datetime=""`
        - `rs:ln rel="up"` vers une capabilitylist.xml (discovery)
    - discovery:
        - capabalitylist liste ce qu'une source est capable de faire, peut être différent pour chaque set
        - toutes les capabilitylists sont dans une Source Description
        - `rs:md capability="capabilitylist"` ou `resourcelist`, `changelist-notification`, …
        - `rs:ln rel="hub"` vers le pubsubhubbub
        - `rs:ln rel="up"` ave une URL vers `.well-known/resourcesync`
        - le lien vers le "up" pourrait être dans le HEADER si on a pas accès au root du server
    - linking to related resources:
        - pointer vers des miroirs, des patch, metadata, …
        - lier vers des représentation alternatives
            - `rs:ln rel="alternate" type="text/html" href=""`
            - `rs:ln rel="canonical" href=""`
        - se rapproche des techniques du signposting

## Implementing RS as part of CORE

https://core.ac.uk/   
https://publisher-connector.core.ac.uk/resourcesync/.well-known/resourcesync

- 77 mio de metadata records
- 6 mio de full text
- ont fait une étude près des éditeurs sur l'accès automatique aux contenus OA, en résumé il n'y a pas d'interopérabilité
- création de dev un publisher connector, avec un connector par éditeur
- quel protocol? OAI-PMH ne fonctionne pas très bien. Donc RS
- voir core.ac.uk et chercher resourcesync, public connector
- puis utilisation de resourcesync pour exposer les données de CORE
- pousser les IR et les revues à implémenter ResourceSync
    - en parallèle à PMH
    - conseillé par COAR Next Gen IR WG
