# OAI10

Mes notes de la conférence OAI10 (2017)

1. [The SignPosting & ResourceSync Workshop (2017-06-21, AM)](signposting-resourcesync-workshop.md)
1. [The 2017-06-21 PM Sessions](2017-06-21-afternoon.md)
1. [The 2017-06-22 Sessions](2017-06-22.md)
1. [The 2017-06-23 Sessions](2017-06-23.md)

@grolimur opened [some pads for his own notes](https://mypads.framapad.org/mypads/?/mypads/group/oai10-u71exy75i/pad/view/oai10-wednesday-9g1eyy7sd) 
